package com.example.codingtest.viewmodel

import com.example.codingtest.network.response.HighSchoolDirectoriesResponseItem
import com.example.codingtest.network.response.SATDataResponseItem


interface MainRepository {
   suspend fun getAllSchools():List<HighSchoolDirectoriesResponseItem>

   suspend fun getSATData(): List<SATDataResponseItem>
}