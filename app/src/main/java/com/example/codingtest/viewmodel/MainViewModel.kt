package com.example.codingtest.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.codingtest.network.response.HighSchoolDirectoriesResponseItem
import kotlinx.coroutines.launch

class MainViewModel : ViewModel() {
     var highSchoolDirectoriesLiveData : MutableLiveData<List<HighSchoolDirectoriesResponseItem>> = MutableLiveData()
     private var userName: String? = null
     private val repository = MainRepositoryImpl()


    fun setUserName(name: String?) {
        userName = name ?: "krishna"
    }

    fun getHighSchoolDirectories() {
        viewModelScope.launch {
            highSchoolDirectoriesLiveData.postValue(repository.getAllSchools())
        }
    }
}