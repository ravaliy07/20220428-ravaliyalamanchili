package com.example.codingtest.viewmodel

import com.example.codingtest.network.RetrofitInstance
import com.example.codingtest.network.response.ReposResponseItem

class MainRepositoryImpl: MainRepository {
    override suspend fun getAllSchools(): List<ReposResponseItem> {
       return RetrofitInstance.api.getAllSchools()
    }
}