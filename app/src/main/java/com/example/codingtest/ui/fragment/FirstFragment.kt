package com.example.codingtest.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.example.codingtest.databinding.FragmentFirstBinding
import com.example.codingtest.ui.adapter.SchoolDirectoriesAdapter
import com.example.codingtest.viewmodel.MainViewModel


class FirstFragment : Fragment() {

    private lateinit var binding:FragmentFirstBinding
    private val viewModel: MainViewModel by activityViewModels()
    private lateinit var navController: NavController
    private lateinit var adapter: SchoolDirectoriesAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentFirstBinding.inflate(inflater,container,false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.navController = Navigation.findNavController(view);
        viewModel.getHighSchoolDirectories()

        viewModel.highSchoolDirectoriesLiveData.observe(viewLifecycleOwner){
            adapter = SchoolDirectoriesAdapter()
            adapter.setItems(it)
            binding.recyclerView.adapter = adapter
            binding.loader.visibility = View.GONE
        }
    }
}