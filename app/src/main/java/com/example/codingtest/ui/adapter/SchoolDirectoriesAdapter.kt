package com.example.codingtest.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.codingtest.R
import com.example.codingtest.network.response.HighSchoolDirectoriesResponseItem

class SchoolDirectoriesAdapter : RecyclerView.Adapter<SchoolDirectoriesAdapter.ItemViewHolder>() {
    private lateinit var items:List<HighSchoolDirectoriesResponseItem>
    class ItemViewHolder(itemView: View):RecyclerView.ViewHolder(itemView){

        fun bindItem(item: HighSchoolDirectoriesResponseItem) {
            val userName = itemView.findViewById<TextView>(R.id.userName)
            val email = itemView.findViewById<TextView>(R.id.email)
            val total_students = itemView.findViewById<TextView>(R.id.total_students)
            val website = itemView.findViewById<TextView>(R.id.website)
            val zipCode = itemView.findViewById<TextView>(R.id.zipcode)
            userName.text = "School Name: ${item.school_name}"
            email.text = "School Email: ${item.school_email}"
            total_students.text ="Total Students: ${item.total_students}"
            website.text = "Website: ${item.website}"
            zipCode.text = "Zip Code: ${item.zip}"
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val item = inflater.inflate(R.layout.item_row,parent,false)
        return ItemViewHolder(item)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bindItem(items[position])
    }

    fun setItems(items: List<HighSchoolDirectoriesResponseItem>) {
        this.items = items
    }

    override fun getItemCount(): Int {
       return items.size
    }
}