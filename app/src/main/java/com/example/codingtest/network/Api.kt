package com.example.codingtest.network

import com.example.codingtest.network.response.HighSchoolDirectoriesResponseItem
import com.example.codingtest.network.response.SATDataResponseItem
import retrofit2.http.GET

interface Api {
    @GET("DOE-High-School-Directory-2017/s3K6-pzi2")
    suspend fun getAllSchools(): List<HighSchoolDirectoriesResponseItem>

    @GET("SAT-Results/f9bf-2cp4")
    suspend fun getSATScores(): List<SATDataResponseItem>
}